from django.urls import path
from .api_views import list_customer, show_customer, list_salesperson, show_salesperson, list_sale, show_sale


urlpatterns = [
    path("customers/", list_customer, name="create_customer"),
    path("customers/<int:id>", show_customer, name="show_customer"),
    path('salespeople/', list_salesperson, name="create_salesperson"),
    path('salespeople/<int:id>', show_salesperson, name="show_salesperson"),
    path('sales/', list_sale, name="create_sale"),
    path('sales/<int:id>', show_sale, name="show_sale")
]
