from django.shortcuts import render
from .models import Customer, Sale, Salesperson, AutomobileVO
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from .encoders import CustomerListEncoder, SalespersonListEncoder, SaleDetailEncoder


@require_http_methods(["GET", "POST"])
def list_customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customers": customer},
            encoder=CustomerListEncoder
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerListEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "NOT VALID"}
            )


@require_http_methods(["GET", "DELETE"])
def show_customer(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                {"customer": customer},
                CustomerListEncoder
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "INVALID"}
            )
    elif request.method == "DELETE":
        customer = Customer.objects.get(id=id)
        customer.delete()
        return JsonResponse(
            customer,
            encoder=CustomerListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def list_salesperson(request, automobile_vo_id=None):
    if request.method == "GET":
        if automobile_vo_id is not None:
            salesperson = Salesperson.objects.filter(id=automobile_vo_id)
        else:
            salesperson = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salesperson},
            encoder=SalespersonListEncoder
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                {"salespeople": salesperson},
                encoder=SalespersonListEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "INVALID"}
            )


@require_http_methods(["GET", "DELETE"])
def show_salesperson(request, id):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=id)
            return JsonResponse(
                {"salespeople": salesperson},
                SalespersonListEncoder
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "INVALID"}
            )
    elif request.method == "DELETE":
        salesperson = Salesperson.objects.get(id=id)
        salesperson.delete()
        return JsonResponse(
            salesperson,
            encoder=SalespersonListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def list_sale(request, vin=None):
    if request.method == "GET":
        if vin is not None:
            sale = Sale.objects.filter(id=vin)
        else:
            sale = Sale.objects.all()
        return JsonResponse(
            {"sales": sale},
            encoder=SaleDetailEncoder
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            employee_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=employee_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "INVALID SALESPERSON"}
            )
        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                 {"message": "INVALID CUSTOMER"}
            )
        try:
            vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "INVALID AUTO"}
            )
        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def show_sale(request, id):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=id)
            return JsonResponse(
                {"sales": sale},
                encoder=SaleDetailEncoder,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "INVALID"}
            )
    elif request.method == "DELETE":
        sale = Sale.objects.get(id=id)
        sale.delete()
        return JsonResponse(
            sale,
            encoder=SaleDetailEncoder,
            safe=False,
        )
