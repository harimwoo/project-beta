import React, { useState, useEffect } from 'react'

function SalesHistory(props){
    const [sales, setSales] = useState([])
    const [salespeople, setSalespeople] = useState([])
    const [unfilteredSales, setUnfilteredSales] = useState([])
    const [salesperson, setSalesPerson] = useState('')

    const handleSalesPersonChange = (event) => {
      const value = event.target.value
      setSalesPerson(value)
    }

    const fetchSalesData = async () => {
        const url = 'http://localhost:8090/api/sales'
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json()
            const allSales = data.sales

            setSales(allSales)
            setUnfilteredSales(allSales)
        }
    }

    useEffect(()=>{
        fetchSalesData()
    }, [])

    const fetchSalespeopleData = async () => {
        const url = 'http://localhost:8090/api/salespeople'
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json()
            const salespeople = data.salespeople
            setSalespeople(salespeople)
        }
    }

    useEffect(()=>{
        fetchSalespeopleData()
    }, [])

    useEffect(()=>{
      const filteredSalesPerson = unfilteredSales.filter(sale => sale.salesperson.employee_id === salesperson)
      setSales(filteredSalesPerson)
    }, [salesperson])

    return (
        <div>
            <div className='mb-3'>
                <select onChange={handleSalesPersonChange} value={salesperson}>
                  <option value="">Select a salesperson</option>
                  {salespeople.map(salesperson => {
                      return(
                        <option value={salesperson.employee_id} key={salesperson.id}>{salesperson.first_name} {salesperson.last_name} </option>
                      )
                  })}
                </select>
              </div>
            <div>
            <table className="table table-striped">
            <thead>
                <tr>
                    <th>Salesperson Name</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
            {sales.map(sale => {
                return(
                    <tr key={ sale.id }>
                        <td> { sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                        <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                        <td>{ sale.automobile.vin }</td>
                        <td>${ sale.price }</td>
                    </tr>
                )
            })}
            </tbody>
        </table>
            </div>
        </div>
      );
    }

export default SalesHistory;
