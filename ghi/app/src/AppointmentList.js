import React, { useState, useEffect } from 'react'

function AppointmentList(props){
    const [appointments, setAppointments] = useState([])
    const [vins, setVins] = useState([])

    const fetchAppointmentData = async () => {
        const url = 'http://localhost:8080/api/appointments'
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json()
            const allAppointments = data.appointments
            const pendingAppointments = []
            for(let appointment of allAppointments){
                if(!appointment.status){
                    pendingAppointments.push(appointment)
                }
            }
            setAppointments(pendingAppointments)
        }
    }

    const fetchVinData = async () => {
        const url = 'http://localhost:8080/api/automobilevos'
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json()
            const autos = data.automobileVOs
            const vins = []
            for(let auto of autos){
                vins.push(auto.vin)
            }
            setVins(vins)
        }
    }

    useEffect(()=>{
        fetchAppointmentData()
    }, [])

    useEffect(()=>{
        fetchVinData()
    }, [])

    const handleCancel = async (appointment_id) => {
        const url = `http://localhost:8080/api/appointments/${appointment_id}/cancel/`
        const response = await fetch(url, {method:'put'})
        if(response.ok){
            const changed = await response.json()
            setAppointments(appointments.filter(appointment => appointment.id !== appointment_id ))
        }
    }

    const handleFinish = async(appointment_id) => {
        const url = `http://localhost:8080/api/appointments/${appointment_id}/finish/`
        const response = await fetch(url, {method:'put'})
        if(response.ok){
            const changed = await response.json()
            setAppointments(appointments.filter(appointment => appointment.id !== appointment_id ))
        }
    }

    return(
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            {appointments.map(appointment => {
                const date = new Date(appointment.date_time).toISOString().slice(0, 10)
                const time = new Date(appointment.date_time).toISOString().slice(11, 16)
                const vip = vins.includes(appointment.vin)
                return(
                    <tr key={ appointment.id }>
                        <td>{ appointment.vin }</td>
                        <td>{ vip ? "Yes":"No" }</td>
                        <td>{ appointment.customer }</td>
                        <td>{ date }</td>
                        <td>{ time }</td>
                        <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                        <td>{ appointment.reason }</td>
                        <td colSpan="7">
                            <button className="btn btn-sm btn-danger" onClick={() => handleCancel(appointment.id)} >Cancel</button>
                            <button className="btn btn-sm btn-success" onClick={() => handleFinish(appointment.id)}>Finish</button>
                        </td>
                    </tr>
                )
            })}
            </tbody>
        </table>
    )
}

export default AppointmentList
