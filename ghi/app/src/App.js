import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import ServiceHistory from './ServiceHistory';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalesPersonList from './SalespersonList';
import SalesPersonForm from './SalespersonForm';
import SaleList from './SaleList';
import SaleForm from './SaleForm';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import ModelList from './ModelList';
import ModelForm from './ModelForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import SalesHistory from './SalesHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route path="list" element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>
          <Route path="models">
            <Route path="list" element={<ModelList />} />
            <Route path="new" element={<ModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route path="list" element={<AutomobileList />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path="technicians">
              <Route path="new" element={<TechnicianForm />} />
              <Route path="list" element={<TechnicianList />} />
          </Route>
          <Route path="appointments">
            <Route path="new" element={<AppointmentForm />} />
            <Route path="list" element={<AppointmentList />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>
          <Route path="customers">
              <Route path="new" element={<CustomerForm />} />
              <Route path="list" element={<CustomerList />} />
          </Route>
          <Route path="salespeople">
              <Route path="new" element={<SalesPersonForm />} />
              <Route path="list" element={<SalesPersonList />} />
          </Route>
          <Route path="sales">
              <Route path="new" element={<SaleForm />} />
              <Route path="list" element={<SaleList />} />
              <Route path="history" element={<SalesHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
