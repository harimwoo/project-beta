import React, { useState, useEffect } from 'react'

function TechnicianForm(props){
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [employeeID, setEmployeeID] = useState('')
    const [errorMessage, setErrorMessage] = useState('')

    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)
    }
    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }
    const handleEmployeeIDChange = (event) => {
        const value = event.target.value
        setEmployeeID(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.first_name = firstName
        data.last_name = lastName
        data.employee_id = employeeID

        const url = 'http://localhost:8080/api/technicians/'
        const config = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, config)
        if(response.ok){
            setEmployeeID('')
            setFirstName('')
            setLastName('')
            setErrorMessage('')
        } else{
            const data = await response.json()
            setErrorMessage(data.message)
        }

    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Technician</h1>
              <form onSubmit={handleSubmit} id="create-technician-form">
                <div className="form-floating mb-3">
                  <input onChange={handleFirstNameChange} placeholder="FirstName" required type="text" name="first_name" id="first_name" className="form-control" value={firstName} />
                  <label htmlFor="first_name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleLastNameChange} placeholder="LastName" required type="text" name="last_name" id="last_name" className="form-control" value={lastName} />
                  <label htmlFor="last_name">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleEmployeeIDChange} placeholder="EmployeeID" required type="text" name="employee_ID" id="employee_ID" className="form-control" value={employeeID} />
                  <label htmlFor="employee_ID">Employee ID</label>
                </div>
                <div>
                  {errorMessage && (
                    <div className="alert alert-danger" role="alert">
                      {errorMessage}
                    </div>
                  )}
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
}

export default TechnicianForm
