import React, { useState, useEffect } from 'react'

function AppointmentForm(props){
    const [technicians, setTechnicians] = useState([])

    const [vin, setVin] = useState('')
    const [customer, setCustomer] = useState('')
    const [dateTime, setDateTime] = useState('')
    const [technician, setTechnician] = useState('')
    const [reason, setReason] = useState('')
    const [errorMessage, setErrorMessage] = useState('')

    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value
        setCustomer(value)
    }
    const handleDateTimeChange = (event) => {
        const value = event.target.value
        setDateTime(value)
    }
    const handleTechnicianChange = (event) => {
        const value = event.target.value
        setTechnician(value)
    }
    const handleReasonChange = (event) => {
        const value = event.target.value
        setReason(value)
    }

    const fetchTechnicianData = async () => {
        const url = 'http://localhost:8080/api/technicians'
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json()
            setTechnicians(data.technicians)
        }
    }

    useEffect(() => {
      fetchTechnicianData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.reason = reason
        data.vin = vin
        data.customer = customer
        data.technician = technician
        data.date_time = dateTime

        const url = 'http://localhost:8080/api/appointments/'
        const config = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, config)
        if(response.ok){
            setVin('')
            setCustomer('')
            setDateTime('')
            setTechnician('')
            setReason('')
            setErrorMessage('')
        } else{
            const data = await response.json()
            setErrorMessage(data.message)
        }

    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a service appointment</h1>
              <form onSubmit={handleSubmit} id="create-appointment-form">
                <div className="form-floating mb-3">
                  <input onChange={handleVinChange} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" value={vin}  />
                  <label htmlFor="vin">Automobile Vin</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleCustomerChange} placeholder="customer" required type="text" name="customer" id="customer" className="form-control" value={customer} />
                  <label htmlFor="customer">Customer</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleDateTimeChange} placeholder="dateTime" required type="datetime-local" name="dateTime" id="dateTime" className="form-control" value={dateTime}/>
                  <label htmlFor="dateTime">Date Time</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleReasonChange} placeholder="reason" required type="text" name="reason" id="reason" className="form-control" value={reason}/>
                  <label htmlFor="reason">Reason</label>
                </div>
                <div className="mb-3">
                        <select onChange={handleTechnicianChange} value={technician} required name="technician" id="technician" className="form-select">
                            <option value="">Choose a Technician</option>
                            {technicians.map(technician => {
                                return(
                                    <option value={technician.employee_id} key={technician.employee_id}>{technician.first_name} {technician.last_name}</option>
                                )
                            })}
                        </select>
                </div>
                <div>
                  {errorMessage && (
                    <div className="alert alert-danger" role="alert">
                      {errorMessage}
                    </div>
                  )}
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
}

export default AppointmentForm
