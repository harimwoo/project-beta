import React, { useEffect, useState } from 'react'

function SalesPersonList(props){
    const [salespeople, setSalesPeople] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json()
            setSalesPeople(data.salespeople)
        }
    }

    useEffect(()=>{
        fetchData()
    }, [])

    return(
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
            </thead>
            <tbody>
            {salespeople.map(salesperson => {
                return(
                    <tr key={ salesperson.employee_id }>
                        <td>{ salesperson.employee_id }</td>
                        <td>{ salesperson.first_name }</td>
                        <td>{ salesperson.last_name }</td>
                    </tr>
                )
            })}
            </tbody>
        </table>
    )
}

export default SalesPersonList