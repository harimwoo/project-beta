import React, { useState, useEffect } from 'react'

function SaleForm(props){
    const [customers, setCustomers] = useState([])
    const [salespeople, setSalesPeople] = useState([])
    const [autos, setAutos] = useState([])

    const [automobile, setAutomobile] = useState('')
    const [customer, setCustomer] = useState('')
    const [salesperson, setSalesPerson] = useState('')
    const [price, setPrice] = useState('')

    const handleAutoChange = (event) => {
        const value = event.target.value
        setAutomobile(value)
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value
        setCustomer(value)
    }
    const handleSalesPersonChange = (event) => {
        const value = event.target.value
        setSalesPerson(value)
    }
    const handlePriceChange = (event) => {
        const value = event.target.value
        setPrice(value)
    }

    const fetchSalesPeopleData = async () => {
        const url = 'http://localhost:8090/api/salespeople'
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json()
            setSalesPeople(data.salespeople)
        }
    }

    useEffect(() => {
        fetchSalesPeopleData()
    }, [])

    const fetchCustomerData = async () => {
        const url = 'http://localhost:8090/api/customers'
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json()
            setCustomers(data.customers)
        }
    }

    useEffect(() => {
        fetchCustomerData()
    }, [])

    const fetchAutoData = async () => {
        const url = 'http://localhost:8100/api/automobiles/'
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json()
            setAutos(data.autos)
        }
    }

    useEffect(() => {
        fetchAutoData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.automobile = automobile
        data.customer = customer
        data.salesperson = salesperson
        data.price = price
        const url = 'http://localhost:8090/api/sales/'
        const config = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(url, config)
        if(response.ok){
            const autoData = {}
            autoData.sold = true
            const autoURL = `http://localhost:8100/api/automobiles/${automobile}/`
            const config = {
                method: 'put',
                body: JSON.stringify(autoData),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            const autoResponse = await fetch(autoURL, config)
            if (autoResponse.ok) {
                setAutomobile('')
                setCustomer('')
                setSalesPerson('')
                setPrice('')
            }
        }

    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a Sale</h1>
              <form onSubmit={handleSubmit} id="create-sale-form">
                <div className="mb-3">
                        <select onChange={handleAutoChange} value={automobile} required name="automobile" id="automobile" className="form-select">
                            <option value="">Choose a Vin Number</option>
                            {autos.map(automobile => {
                                return(
                                    <option value={automobile.vin} key={automobile.id}> {automobile.vin}</option>
                                )
                            })}
                        </select>
                </div>
                <div className="mb-3">
                        <select onChange={handleCustomerChange} value={customer} required name="customer" id="customer" className="form-select">
                            <option value="">Choose a Customer</option>
                            {customers.map(customer => {
                                return(
                                    <option value={customer.id} key={customer.id}>{customer.first_name} {customer.last_name} {customer.phone_number}</option>
                                )
                            })}
                        </select>
                </div>
                <div className="mb-3">
                        <select onChange={handleSalesPersonChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                            <option value="">Choose a SalesPerson</option>
                            {salespeople.map(salesperson => {
                                return(
                                    <option value={salesperson.id} key={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                                )
                            })}
                        </select>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePriceChange} placeholder="Price" required type="text" name="price" id="price" className="form-control" value={price}/>
                  <label htmlFor="price">Price</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
}

export default SaleForm
