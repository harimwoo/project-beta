import React, { useState, useEffect } from 'react'

function SaleList(props){
    const [sales, setSales] = useState([])

    const fetchSalesData = async () => {
        const url = 'http://localhost:8090/api/sales'
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json()
            setSales(data.sales)
        }
    }

    useEffect(()=>{
        fetchSalesData()
    }, [])

    return(
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>SalesPerson Employee ID</th>
                    <th>SalesPerson Name</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
            {sales.map(sale => {
                return(
                    <tr key={ sale.id }>
                        <td>{ sale.salesperson.employee_id }</td>
                        <td> { sale.salesperson.first_name } {sale.salesperson.last_name}</td>
                        <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                        <td>{ sale.automobile.vin }</td>
                        <td>${ sale.price }</td>
                    </tr>
                )
            })}
            </tbody>
        </table>
    )
}

export default SaleList
