import React, { useState, useEffect } from 'react'

function ModelForm(props){
    const [manufacturers, setManufacturers] = useState([])
    const [errorMessage, setErrorMessage] = useState('')

    const [name, setName] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')
    const [manufacturer, setManufacturer] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value
        setPictureUrl(value)
    }
    const handleManufacturerChange = (event) => {
        const value = event.target.value
        setManufacturer(value)
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers'
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.name = name
        data.manufacturer_id = manufacturer
        data.picture_url = pictureUrl
        const url = 'http://localhost:8100/api/models/'
        const config = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, config)
        if(response.ok){
            setName('')
            setPictureUrl('')
            setManufacturer('')
            setErrorMessage('')
        } else {
            setErrorMessage("INVALID URL")
        }
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a Model</h1>
              <form onSubmit={handleSubmit} id="create-appointment-form">
                <div className="form-floating mb-3">
                  <input onChange={handleNameChange} placeholder="name" required type="text" name="name" id="name" className="form-control" value={name}  />
                  <label htmlFor="name">Model Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePictureUrlChange} placeholder="pictureUrl" required type="text" name="pictureUrl" id="pictureUrl" className="form-control" value={pictureUrl} />
                  <label htmlFor="pictureUrl">Picture URL</label>
                </div>
                <div>
                  {errorMessage && (
                    <div className="alert alert-danger" role="alert">
                      {errorMessage}
                    </div>
                  )}
                </div>
                <div className="mb-3">
                        <select onChange={handleManufacturerChange} value={manufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                            <option value="">Choose a Manufacturer</option>
                            {manufacturers.map(manufacturer => {
                                return(
                                    <option value={manufacturer.id} key={manufacturer.id}>{ manufacturer.name }</option>
                                )
                            })}
                        </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
}

export default ModelForm
