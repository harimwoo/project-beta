import React, { useState, useEffect } from 'react'

function CustomerForm(props) {
  
    const [first_name, setFirstName] = useState('')
    const [last_name, setLastName] = useState('')
    const [address, setAddress] = useState('')
    const [phone_number, setPhoneNumber] = useState('')
    const [errorMessage, setErrorMessage] = useState('')

    const handleFirstNameChange = (event) => {
        const value = event.target.value
        setFirstName(value)
    }
    const handleLastNameChange = (event) => {
        const value = event.target.value
        setLastName(value)
    }
    const handleAddressChange = (event) => {
        const value = event.target.value
        setAddress(value)
    }
    const handlePhoneNumberChange = (event) => {
        const value = event.target.value
        setPhoneNumber(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.first_name = first_name
        data.last_name = last_name
        data.address = address
        data.phone_number = phone_number

        const url = 'http://localhost:8090/api/customers/'
        const config = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, config)
        if(response.ok){
            setFirstName('')
            setLastName('')
            setAddress('')
            setPhoneNumber('')
            setErrorMessage('')
        } else{
            const data = await response.json()
            setErrorMessage(data.message)
        }

    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a New Customer </h1>
              <form onSubmit={handleSubmit} id="create-customer-form">
                <div className="form-floating mb-3">
                  <input onChange={handleFirstNameChange} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" value={first_name} />
                  <label htmlFor="first_name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleLastNameChange} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" value={last_name}/>
                  <label htmlFor="last_name">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleAddressChange} placeholder="Address" required type="text" name="address" id="address" className="form-control" value={address}/>
                  <label htmlFor="address">Address</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePhoneNumberChange} placeholder="Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control" value={phone_number}/>
                  <label htmlFor="phone_number">Phone Number</label>
                </div>
                <div>
                  {errorMessage && (
                    <div className="alert alert-danger" role="alert">
                      {errorMessage}
                    </div>
                  )}
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
}

export default CustomerForm