import React, { useState, useEffect } from 'react'

function ModelList(props){
    const [models, setModels] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models'
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json()
            setModels(data.models)
        }
    }

    const handleCancel = async (model_id) => {
        const url = `http://localhost:8100/api/manufacturers/${model_id}`
        const response = await fetch(url, {method:'delete'})
        if(response.ok){
            setModels(models.filter(model => model.id !== model_id ))
        }
    }

    useEffect(()=>{
        fetchData()
    }, [])

    return(
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            {models.map(model => {
                return(
                    <tr key={ model.id }>
                        <td>{ model.name }</td>
                        <td>{ model.manufacturer.name }</td>
                        <td><img className="img-thumbnail" src={ model.picture_url } /></td>
                        <td><button className="btn btn-sm btn-danger" onClick={() => handleCancel(model.id)}>X</button></td>
                    </tr>
                )
            })}
            </tbody>
        </table>
    )
}

export default ModelList
