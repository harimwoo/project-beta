import React, { useEffect, useState } from 'react'

function TechnicianList(props){
    const [technicians, setTechnicians] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians'
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json()
            setTechnicians(data.technicians)
        }
    }

    const handleCancel = async (employee_id) => {
        const url = `http://localhost:8080/api/technicians/${employee_id}`
        const response = await fetch(url, {method:'delete'})
        if(response.ok){
            setTechnicians(technicians.filter(technician => technician.employee_id !== employee_id ))
        }
    }
    useEffect(()=>{
        fetchData()
    }, [])

    return(
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            {technicians.map(technician => {
                return(
                    <tr key={ technician.employee_id }>
                        <td>{ technician.employee_id }</td>
                        <td>{ technician.first_name }</td>
                        <td>{ technician.last_name }</td>
                        <td><button className="btn btn-sm btn-danger" onClick={() => handleCancel(technician.employee_id)} >X</button></td>
                    </tr>
                )
            })}
            </tbody>
        </table>
    )
}

export default TechnicianList
