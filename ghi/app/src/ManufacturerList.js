import React, { useState, useEffect } from 'react'

function ManufacturerList(props){
    const [manufacturers, setManufacturers] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers'
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }
    }
    const handleCancel = async (manufacturer_id) => {
        const url = `http://localhost:8100/api/manufacturers/${manufacturer_id}`
        const response = await fetch(url, {method:'delete'})
        if(response.ok){
            setManufacturers(manufacturers.filter(manufacturer => manufacturer.id !== manufacturer_id ))
        }
    }

    useEffect(()=>{
        fetchData()
    }, [])

    return(
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
            {manufacturers.map(manufacturer => {
                return(
                    <tr key={ manufacturer.id }>
                        <td>{ manufacturer.name }</td>
                        <td><button className="btn btn-sm btn-danger" onClick={() => handleCancel(manufacturer.id)}>X</button></td>
                    </tr>
                )
            })}
            </tbody>
        </table>
    )
}

export default ManufacturerList
