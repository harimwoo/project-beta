import React, { useState, useEffect } from 'react'

function ServiceHistory(props){
    const [appointments, setAppointments] = useState([])
    const [vins, setVins] = useState([])
    const [search, setSearch] = useState('')
    const [unfilteredAppointments, setUnfilteredAppointments] = useState([])

    const handleSearchChange = (event) => {
        const value = event.target.value
        setSearch(value)
    }

    useEffect(()=>{
        const filteredAppointments = unfilteredAppointments.filter(appointment => appointment.vin.includes(search))
        setAppointments(filteredAppointments)
    }, [search])

    const fetchAppointmentData = async () => {
        const url = 'http://localhost:8080/api/appointments'
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json()
            const allAppointments = data.appointments
            const appointmentCorrected = []
            for(let appointment of allAppointments){
                if(!appointment.status){
                    appointment.status = 'created'
                }
                appointmentCorrected.push(appointment)
            }
            setAppointments(appointmentCorrected)
            setUnfilteredAppointments(appointmentCorrected)
        }
    }

    useEffect(()=>{
        fetchAppointmentData()
    }, [])

    const fetchVinData = async () => {
        const url = 'http://localhost:8100/api/automobiles'
        const response = await fetch(url)
        if(response.ok){
            const data = await response.json()
            const autos = data.autos
            const vins = []
            for(let auto of autos){
                vins.push(auto.vin)
            }
            setVins(vins)
        }
    }

    useEffect(()=>{
        fetchVinData()
    }, [])

    return(
        <div>
            <input type="text" placeholder='Search by VIN...' onChange={handleSearchChange} className="form-control form-control-lg" />
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                {appointments.map(appointment => {
                    const date = new Date(appointment.date_time).toISOString().slice(0, 10)
                    const time = new Date(appointment.date_time).toISOString().slice(11, 16)
                    const vip = vins.includes(appointment.vin)
                    return(
                        <tr key={ appointment.id }>
                            <td>{ appointment.vin }</td>
                            <td>{ vip ? "Yes":"No" }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ date }</td>
                            <td>{ time }</td>
                            <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                            <td>{ appointment.reason }</td>
                            <td>{ appointment.status }</td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
        </div>
    )
}

export default ServiceHistory
