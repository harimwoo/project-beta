# CarCar

Team:

* Harim Woo - Service
* Joseph Keene - Sales

## Design

## Service microservice

*Models*
Service microservice has three models:

(1) Technician
    - first_name
    - last_name
    - employee_id (must be unique)

(2) Appointment:
    - date_time
    - reaston
    - status (null when unspecified / can be switched to "canceled" or "finished" via front-end)
    - vin
    - customer
    - technician (uses foreign key to the Technician model above)

(3) AutomobileVO:
    - vin

*Integration*
(A) AutomobileVO model in Service microservice polls Automobile data from the Inventory every 15 seconds.
(B) When a new instance of Appointment is created, the input vin is checked in the Inventory database.
    If the input vin exists in the Inventory database, it is determined that the vehicle was purchased from our dealership.
(C) Once it is determined that the vehicle of interest was purchased from the inventory, the customer from (B) is assigned a
    VIP status. It is visible under "Service Appointments" tab on the front-end.


## Sales microservice

Explain your models and integration with the inventory
microservice, here.
