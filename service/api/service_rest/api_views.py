from .models import AutomobileVO, Technician, Appointment
from django.views.decorators.http import require_http_methods
from django.db import IntegrityError
from django.http import JsonResponse
from .encoders import TechnicianEncoder, AutomobileVOEncoder, AppointmentEncoder
import json


@require_http_methods(["GET"])
def list_automobileVO(request):
    automobilesVOs = AutomobileVO.objects.all()
    return JsonResponse({"automobileVOs": automobilesVOs}, AutomobileVOEncoder)


@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse({"technicians": technicians}, TechnicianEncoder)
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse({"technician": technician}, TechnicianEncoder)
        except IntegrityError:
            return JsonResponse({"message": "employee id already exists"}, status=400)


@require_http_methods(["DELETE"])
def delete_technician(request, employee_id):
    count, _ = Technician.objects.filter(employee_id=employee_id).delete()
    if count > 0:
        return JsonResponse({"deleted": count > 0}, status=200)
    else:
        return JsonResponse({"message": "unknown technician"}, status=400)


@require_http_methods(["GET", "POST"])
def list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse({"appointments": appointments}, AppointmentEncoder)
    else:
        content = json.loads(request.body)
        try:
            employee_id = content["technician"]
            technician = Technician.objects.get(employee_id=employee_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "INVALID TECHNICIAN"},
                status=400,
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse({"appointment": appointment}, AppointmentEncoder)


@require_http_methods(["DELETE"])
def delete_appointment(request, id):
    count, _ = Appointment.objects.filter(id=id).delete()
    if count > 0:
        return JsonResponse({"deleted": count > 0}, status=200)
    else:
        return JsonResponse({"message": "unknown appointment"}, status=400)


@require_http_methods(["PUT"])
def cancel_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.status = "canceled"
    appointment.save()
    return JsonResponse({"status": "canceled"}, AppointmentEncoder)


@require_http_methods(["PUT"])
def finish_appointment(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.status = "finished"
    appointment.save()
    return JsonResponse({"status": "finished"}, AppointmentEncoder)
