from django.urls import path
from .api_views import (
    list_automobileVO,
    list_technicians,
    delete_technician,
    list_appointments,
    delete_appointment,
    cancel_appointment,
    finish_appointment
)

urlpatterns = [
    path("automobilevos/", list_automobileVO, name="list_automobileVO"),
    path("technicians/", list_technicians, name="list_technicians"),
    path("technicians/<str:employee_id>/", delete_technician, name="delete_technician"),
    path("appointments/", list_appointments, name="list_appointments"),
    path("appointments/<int:id>/", delete_appointment, name="delete_appointment"),
    path("appointments/<int:id>/cancel/", cancel_appointment, name="cancel_appointment"),
    path("appointments/<int:id>/finish/", finish_appointment, name="finish_appointment")
]
