from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200)


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.employee_id


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField(blank=True)
    vin = models.CharField(max_length=200)
    customer = models.CharField(max_length=200)

    status = models.CharField(max_length=200, null=True)

    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE
    )
